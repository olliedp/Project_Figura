﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundVolume : MonoBehaviour
{
    private AudioSource sound;

	// Use this for initialization
	void Start ()
    {
        sound = GetComponent<AudioSource>();
        sound.volume = AudioManager.instance.soundEffectVolume;
	}

    void Update()
    {
        sound.volume = AudioManager.instance.soundEffectVolume;
    }
}
