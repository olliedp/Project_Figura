﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * When the player gets near a pellet, it will be drawn towards the player
 */

public class PlayerAttraction : MonoBehaviour
{
    public GameObject player;

    private ShapeProperty pelletShape, playerShape;

	// Use this for initialization
	void OnEnable()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player");

        pelletShape = GetComponent<ShapeProperty>();
        playerShape = player.GetComponent<ShapeProperty>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(player.activeSelf && pelletShape.currentShape == playerShape.currentShape && Vector2.Distance(transform.position, player.transform.position) <= 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, 0.1f);
        }	
	}
}
