﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionManager : MonoBehaviour
{
    public Text musicText, soundText, backText;

    public Slider musicSlider, soundSlider;

    private AudioSource audio;
    private bool delay;

    public int select = 0;

    // Use this for initialization
    void Start()
    {
        musicText.color = Color.white;
        soundText.color = Color.gray;
        backText.color = Color.gray;

        musicSlider.value = AudioManager.instance.musicVolume;
        soundSlider.value = AudioManager.instance.soundEffectVolume;

        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Confirm") > 0 && select % 3 == 2)
            SceneManager.LoadScene("Main Menu");

        if (Input.GetAxis("Vertical") + Input.GetAxis("Fire Vertical") + 
            Input.GetAxis("Horizontal") + Input.GetAxis("Fire Horizontal") == 0.0f)
        {
            delay = false;
            StopAllCoroutines();
        }
        if (!delay && (Input.GetAxis("Vertical") > 0 || Input.GetAxis("Fire Vertical") > 0))
        {
            audio.Play();
            select = (select <= 0) ? 2 : select - 1;
            Select();
            StartCoroutine(SelectionDelay());
        }
        if (!delay && (Input.GetAxis("Vertical") < 0 || Input.GetAxis("Fire Vertical") < 0))
        {
            audio.Play();
            select++;
            Select();
            StartCoroutine(SelectionDelay());
        }

        if (!delay && (Input.GetAxis("Horizontal") < 0 || Input.GetAxis("Fire Horizontal") < 0))
        {
            ChangeSliderValue(-0.1f);
            StartCoroutine(SelectionDelay());
        }
        if (!delay && (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Fire Horizontal") > 0))
        {
            ChangeSliderValue(0.1f);
            StartCoroutine(SelectionDelay());
        }
    }

    private void Select()
    {
        musicText.color = Color.gray;
        soundText.color = Color.gray;
        backText.color = Color.gray;

        switch (select % 3)
        {
            case 0:
                musicText.color = Color.white;
                break;
            case 1:
                soundText.color = Color.white;
                break;
            case 2:
                backText.color = Color.white;
                break;
        }
    }

    private void ChangeSliderValue(float amount)
    {
        float currentMusic = musicSlider.value;
        float currentSound = soundSlider.value;

        switch(select % 3)
        {
            case 0:
                musicSlider.value = Mathf.Clamp(currentMusic + amount, 0f, 1f);
                AudioManager.instance.musicVolume = musicSlider.value;
                break;

            case 1:
                soundSlider.value = Mathf.Clamp(currentSound + amount, 0f, 1f);
                AudioManager.instance.soundEffectVolume = soundSlider.value;
                break;
        }
    }

    private IEnumerator SelectionDelay()
    {
        delay = true;
        yield return new WaitForSeconds(1f);
        delay = false;
    }
}
