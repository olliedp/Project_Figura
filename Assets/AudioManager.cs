﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;

    public float musicVolume = 0.5f;
    public float soundEffectVolume = 0.5f;

    private AudioSource music;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        music = GetComponent<AudioSource>();
    }

	public void Play()
    {
        music.Play();
    }

    public void Pause()
    {
        music.Pause();
    } 

    public void SetMusic(AudioClip clip)
    {
        music.clip = clip;
    }

    public IEnumerator FadeToSong(float fadeOutDur, float fadeInStart, float fadeInDur, AudioClip song)
    {
        yield return null;
    }
}
