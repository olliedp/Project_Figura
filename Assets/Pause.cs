﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Pause : MonoBehaviour
{
    public static bool paused = false;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (paused)
        {
            Time.timeScale = 0;
            //DOTween.PauseAll();
            DOTween.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            //DOTween.PlayAll();
            DOTween.timeScale = 1;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            paused = !paused;
	}
}
