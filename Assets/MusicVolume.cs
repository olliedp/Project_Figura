﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MusicVolume : MonoBehaviour
{
    private AudioSource music;

	// Use this for initialization
	void Start ()
    {
        music = GetComponent<AudioSource>();
        music.volume = AudioManager.instance.musicVolume;
	}

    public void Update()
    {
        music.volume = AudioManager.instance.musicVolume;
    }
}
