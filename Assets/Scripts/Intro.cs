﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour
{
    new public AudioSource audio;
    public Text text;
    public AudioClip pow;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(ChangeText());    
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator ChangeText()
    {
        yield return new WaitForSeconds(audio.clip.length);
        text.text = "";
        yield return new WaitForSeconds(1.5f);
        text.text = "A GAME\nBY sud";
        audio.Play();
        yield return new WaitForSeconds(audio.clip.length);
        text.text = "";
        yield return new WaitForSeconds(1.5f);
        text.fontSize = 128;
        text.text = "FIGURA";
        audio.clip = pow;
        audio.Play();
        yield return new WaitForSeconds(3f);

        Color textColor = text.color;
        float alpha = text.color.a;
        float elapsed = 0f;

        while(elapsed < 1f)
        {
            elapsed += Time.deltaTime;
            textColor.a = Mathf.Lerp(alpha, 0f, elapsed / 1f);
            text.color = textColor;
            yield return null;
        }
    }
}
