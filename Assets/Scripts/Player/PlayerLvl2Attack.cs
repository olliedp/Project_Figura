﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLvl2Attack : MonoBehaviour, IAttack
{
    private float fireTime = 0f;

    private BulletPool bulletPool;
    private ShapeProperty shape;
    private Color playerColor;

    void Start()
    {
        bulletPool = FindObjectOfType<BulletPool>();
        shape = GetComponentInParent<ShapeProperty>();
        playerColor = GetComponent<SpriteRenderer>().color;
    }

    public void Attack()
    {
        if (gameObject.activeSelf)
        {
            Vector3 left = transform.position - transform.right / 4;
            Vector3 right = transform.position + transform.right / 4;

            bulletPool.FireBullet(shape.currentShape, playerColor, left, transform.rotation, 25, "Player Bullet");
            bulletPool.FireBullet(shape.currentShape, playerColor, right, transform.rotation, 25, "Player Bullet");
        }
    }
}
