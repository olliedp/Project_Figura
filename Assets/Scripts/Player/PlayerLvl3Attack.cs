﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLvl3Attack : MonoBehaviour, IAttack
{
    private float fireTime = 0f;

    private BulletPool bulletPool;
    private ShapeProperty shape;
    private Color playerColor;

    void Start()
    {
        bulletPool = FindObjectOfType<BulletPool>();
        shape = GetComponentInParent<ShapeProperty>();
        playerColor = GetComponent<SpriteRenderer>().color;
    }

    public void Attack()
    {
        if (gameObject.activeSelf)
        {
            var eRotation = transform.rotation.eulerAngles;
            var rotateBy = new Vector3(0, 0, 10f);
            bulletPool.FireBullet(shape.currentShape, playerColor, transform.position, transform.rotation, 25, "Player Bullet");
            bulletPool.FireBullet(shape.currentShape, playerColor, transform.position, Quaternion.Euler(eRotation + rotateBy), 25, "Player Bullet");
            bulletPool.FireBullet(shape.currentShape, playerColor, transform.position, Quaternion.Euler(eRotation - rotateBy), 25, "Player Bullet");
        }
    }
}
