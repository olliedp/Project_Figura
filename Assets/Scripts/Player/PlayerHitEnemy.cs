﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitEnemy : MonoBehaviour
{
    public bool invincible = false;

    private ShapeProperty playerShape;
    private PlayerLives lives;
    private SpriteRenderer sprite;
    private PlayerDeath deathScript;
    private Power power;
    private PelletPool pellets;

    void Start()
    {
        playerShape = GetComponentInParent<ShapeProperty>();
        lives = PlayerLives.instance;
        sprite = transform.parent.GetComponentInChildren<SpriteRenderer>();
        deathScript = FindObjectOfType<PlayerDeath>();
        power = GetComponentInParent<Power>();
        pellets = FindObjectOfType<PelletPool>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            var enemyShape = coll.gameObject.GetComponent<ShapeProperty>();
            if (!lives.invincible && playerShape.currentShape == enemyShape.currentShape)
            {
                playerShape.gameObject.SetActive(false);
                power.power /= 2;
                for (int i = 0; i < power.power; i++)
                {
                    pellets.GetPellet(playerShape.currentShape, transform.position, transform.rotation);
                }

                deathScript.UpdateLives();
            }
        }
    }

    void BackToLife()
    {
        gameObject.SetActive(true);
    }
}
