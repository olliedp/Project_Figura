﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField]
    public GameObject player;
    private PlayerLives lives;
    public SpriteRenderer sprite;
    public Text gameOverText;
    public AudioClip menuMusic;

    // Use this for initialization
    void Start ()
    {
        player = GameObject.Find("Player");

        lives = PlayerLives.instance;
        sprite = player.transform.GetComponentInChildren<SpriteRenderer>();
    }
	
    

	public void UpdateLives()
    {
        PlayerLives.instance.Decrement();
        if (PlayerLives.instance.Count > 0)
        {
            StartCoroutine(BackToLife());
        }
        else
        {
            StartCoroutine(GameOver());
        }

    }

    public IEnumerator BackToLife()
    {
        lives.invincible = true;

        yield return new WaitForSeconds(2f);

        player.SetActive(true);

        float elapsedTime = 0f;

        bool black = false;

        while (elapsedTime < 2f)
        {
            sprite.color = black ? Color.black : Color.white;
            black = !black;
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        sprite.color = Color.white;
        lives.invincible = false;
    }

    private IEnumerator GameOver()
    {
        yield return new WaitForSeconds(2f);
        gameOverText.text = "GAME\nOVER";
        yield return new WaitForSeconds(4f);
        AudioManager.instance.SetMusic(menuMusic);
        AudioManager.instance.Pause();
        SceneManager.LoadScene("Main Menu");
    }
}
