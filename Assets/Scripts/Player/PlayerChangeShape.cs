﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Change the shape of the player by pressing tab

public class PlayerChangeShape : MonoBehaviour
{
    private ShapeProperty shp;
    private int shapeIndex = 0;

    private bool keyPressed;

	// Use this for initialization
	void Start ()
    {
        shp = GetComponent<ShapeProperty>();
        shp.currentShape = Shape.Square;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!keyPressed && Input.GetAxisRaw("Change Shape") != 0)
        {
            keyPressed = true;
            shapeIndex = (Input.GetAxisRaw("Change Shape") > 0) ? shapeIndex + 1 : shapeIndex - 1;
            if(shapeIndex < 0)
                shapeIndex = 2;

            switch (shapeIndex % 3)
            {
                case 0:
                    shp.currentShape = Shape.Square;
                    break;

                case 1:
                    shp.currentShape = Shape.Circle;
                    break;

                case 2:
                    shp.currentShape = Shape.Triangle;
                    break;
            }
        }

        else if(Input.GetAxisRaw("Change Shape") == 0)
            keyPressed = false;

        if (Input.GetAxisRaw("Square") > 0 && shp.currentShape != Shape.Square)
        {
            shp.currentShape = Shape.Square;
            shapeIndex = 0;
        }
        else if (Input.GetAxisRaw("Circle") > 0 && shp.currentShape != Shape.Circle)
        {
            shp.currentShape = Shape.Circle;
            shapeIndex = 1;
        }
        else if (Input.GetAxisRaw("Triangle") > 0 && shp.currentShape != Shape.Triangle)
        {
            shp.currentShape = Shape.Triangle;
            shapeIndex = 2;
        }

	}
}
