﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLives : MonoBehaviour
{
    public static PlayerLives instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    [SerializeField]
    private int lives = 3;

    public bool invincible = false;

    public int Count
    {
        get
        {
            return lives;
        }
    }

    public void Increment()
    {
        lives++;
    }

    public void Decrement()
    {
        lives--;
    }
}
