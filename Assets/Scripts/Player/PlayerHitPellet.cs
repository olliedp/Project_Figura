﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitPellet : MonoBehaviour
{
    private ShapeProperty playerShape;
    private Power playerPower;

	// Use this for initialization
	void Start ()
    {
        playerShape = GetComponentInParent<ShapeProperty>();
        playerPower = GetComponentInParent<Power>();
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Pellet")
        {
            var pelletShape = coll.GetComponent<ShapeProperty>();
            if (playerShape.currentShape == pelletShape.currentShape)
            {
                playerPower.power++;
                pelletShape.gameObject.SetActive(false);
            }
        }
    }
}
