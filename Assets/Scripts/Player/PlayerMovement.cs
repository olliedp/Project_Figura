﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 0.125f / 2;
    public float minX, minY, maxX, maxY;
    
    private float speedX = 0;
    private float speedY = 0;

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.F4))
            Screen.fullScreen = !Screen.fullScreen;

        if (Input.GetAxis("Horizontal") < 0)
            speedX = -moveSpeed;
        else if (Input.GetAxis("Horizontal") > 0)
            speedX = moveSpeed;
        else
            speedX = 0;

        if (Input.GetAxis("Vertical") > 0)
            speedY = moveSpeed;
        else if (Input.GetAxis("Vertical") < 0)
            speedY = -moveSpeed;
        else
            speedY = 0;

        if (Input.GetAxis("SlowDown") > 0)
            moveSpeed = 5f;
        else
            moveSpeed = 7f;

        transform.Translate(speedX * Time.deltaTime, speedY * Time.deltaTime, 0);

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, minX, maxX),
            Mathf.Clamp(transform.position.y, minY, maxY), -0.1f
            );
    }
}
