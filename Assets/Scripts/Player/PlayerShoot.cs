﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public float fireRate;
    private float fireTime = 0f;

    public IAttack[] attacks;
    private IAttack playerAttack;

    private Power power;

    public static bool canShoot = true;

    private AudioSource audio;

    // Use this for initialization
    void Start ()
    {
        attacks = GetComponents<IAttack>();
        playerAttack = attacks[0];

        power = GetComponentInParent<Power>();

        audio = GetComponent<AudioSource>();
	}
    
    // Update is called once per frame
    void Update()
    {
        if (canShoot && (Input.GetAxisRaw("Fire Horizontal") != 0 || Input.GetAxisRaw("Fire Vertical") != 0))
        {
            if (fireTime >= fireRate)
            {
                audio.Play();
                playerAttack.Attack();
                fireTime = 0f;
            }
            else
                fireTime += Time.deltaTime;
        }
        else
            fireTime = 0;

        playerAttack = attacks[power.powerLevel - 1];
    }
}
