﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLvl1Attack : MonoBehaviour, IAttack
{ 
    private float fireTime = 0f;

    public BulletPool bulletPool;

    private ShapeProperty shape;
    private Color playerColor;

    void Start()
    {
        shape = GetComponentInParent<ShapeProperty>();
        playerColor = GetComponent<SpriteRenderer>().color;
    }

    public void Attack()
    {
        if (gameObject.activeSelf)
            bulletPool.FireBullet(shape.currentShape, playerColor, transform.position, transform.rotation, 25, "Player Bullet");
            //bulletPool.Pull(transform.position, transform.rotation, "Player Bullet", shape.currentShape, playerColor, 30);
    }
}
