﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TrackScore : MonoBehaviour
{
    private uint currentScore;

    private bool updating = false;

    private Text text;

	// Use this for initialization
	void Start ()
    {
        currentScore = Score.value;

        text = GetComponent<Text>();
        text.text = currentScore.ToString();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!updating && currentScore < Score.value)
            StartCoroutine(ScoreUpdate());	
	}

    private IEnumerator ScoreUpdate()
    {
        uint start = currentScore;
        uint score = Score.value;

        updating = true;

        float elapsed = 0.0f;

        while (elapsed < 1f)
        {
            currentScore = (uint)Mathf.Lerp(start, Score.value, elapsed / 1f);
            text.text = currentScore.ToString();
            elapsed += Time.deltaTime;
            yield return null;
        }
        currentScore = Score.value;
        text.text = currentScore.ToString();
        updating = false;
    }
}
