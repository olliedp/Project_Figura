﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TrackLives : MonoBehaviour
{
    private Text text;

    private PlayerLives playerLives;
    private int lives;

	// Use this for initialization
	void Start ()
    {
        text = GetComponent<Text>();

        playerLives = PlayerLives.instance;
        lives = playerLives.Count;

        string str = "LIVES: ";
        for(int i = 0; i < lives; i++)
        {
            str += "I ";
        }
        text.text = str;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(playerLives.Count > lives)
        {
            text.text += "I ";
            lives = playerLives.Count;
        }
        else if(playerLives.Count < lives)
        {
            text.text = text.text.Substring(0, text.text.Length - 2);
            lives = playerLives.Count;
        }
	}

    private void SetLivesString()
    {
    }
}
