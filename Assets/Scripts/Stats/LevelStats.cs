﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStats : MonoBehaviour
{
    public static LevelStats instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    public uint currentLevel = 1;

    public bool levelComplete;

    public uint bulletsHit;
    public uint bulletsMissed;
    public float accuracy;

    public float bossTime;
    public bool timerStart = false;

    public uint enemyCount = 50;
    public uint enemyKills = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(bulletsHit > 0 || bulletsMissed > 0)
            accuracy = (float)bulletsHit / (bulletsHit + bulletsMissed) * 100f;

        if (timerStart)
            bossTime += Time.deltaTime;
	}
}
