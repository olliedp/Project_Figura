﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatDisplay : MonoBehaviour
{
    public Text head;
    public Text statText;

    public AudioClip menuMusic;

    private bool displayingText;

    private LevelStats stats;

    void Start()
    {
        stats = LevelStats.instance;
    }

    void Update()
    {
        if(stats.levelComplete && !displayingText)
        {
            displayingText = true;
            StartCoroutine(TextDisplay());
        }
    }

    IEnumerator TextDisplay()
    {
        uint accBonus = (uint)(Mathf.Clamp((stats.accuracy / 80f), 0, 1) * 10000f);
        uint enBonus = (uint)(((stats.enemyKills - 5) / stats.enemyCount) * 10000f);
        uint timeBonus;
        if (stats.bossTime <= 120)
            timeBonus = 10000;
        else if (stats.bossTime >= 180)
            timeBonus = 0;
        else
            timeBonus = (uint)(stats.bossTime / 180 * 10000);
            

        yield return new WaitForSeconds(2f);
        head.enabled = true;

        yield return new WaitForSeconds(1f);
        statText.enabled = true;
        statText.text = "Accuracy:\t" + stats.accuracy + "\t";

        yield return new WaitForSeconds(1f);
        statText.text += "\nEnemies killed:\t" + stats.enemyKills + "/" + stats.enemyCount;

        yield return new WaitForSeconds(1f);
        statText.text += "\nBoss Time:\t" + string.Format("{0:0}:{1:00}", Mathf.Floor(stats.bossTime / 60), stats.bossTime % 60);

        yield return new WaitForSeconds(1f);
        statText.text += "\n\nBonus:\t" + (accBonus + enBonus + timeBonus);
        Score.value += (accBonus + enBonus + timeBonus);

        yield return new WaitForSeconds(5f);
        AudioManager.instance.Pause();
        AudioManager.instance.SetMusic(menuMusic);
        stats.levelComplete = false;
        displayingText = false;
        SceneManager.LoadScene("Main Menu");
    }
}
