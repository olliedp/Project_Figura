﻿using UnityEngine;
using UnityEngine.UI;

public class PowerSlider : MonoBehaviour
{
    Text slideText;
    Slider slider;

    public Power playerPower;
    private int currentLevel, currentPower;

	// Use this for initialization
	void Start ()
    {
        slideText = GetComponentInChildren<Text>();
        slider = GetComponent<Slider>();

        currentLevel = playerPower.powerLevel;
        currentPower = playerPower.power;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (currentPower != playerPower.power)
            SliderUpdate();
	}

    private void SliderUpdate()
    {
        if (playerPower.powerLevel != currentLevel)
            currentLevel = playerPower.powerLevel;

        switch(currentLevel)
        {
            case 1:
                slider.minValue = 0;
                slider.maxValue = 49;
                break;

            case 2:
                slider.minValue = 50;
                slider.maxValue = 149;
                break;

            case 3:
                slider.minValue = 150;
                slider.maxValue = 299;
                break;
        }

        slideText.text = "LVL " + currentLevel;
        slider.value = playerPower.power;
    }
}
