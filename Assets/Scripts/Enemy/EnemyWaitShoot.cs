﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Use this if you only want the enemy to shoot while it's stationary on screen

public class EnemyWaitShoot : MonoBehaviour
{
    public float fireRate;
    private float fireTime = 0f;

    private IAttack enemyAttack;

    private EnemyPath enemyPath;

    // Use this for initialization
    void Start()
    {
        enemyAttack = GetComponent<IAttack>();
        enemyPath = GetComponent<EnemyPath>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (enemyPath.currentStatus == EnemyStatus.Waiting && fireTime > fireRate)
        {
            enemyAttack.Attack();
            fireTime = 0;
        }
        else
            fireTime += Time.deltaTime;
    }
}
