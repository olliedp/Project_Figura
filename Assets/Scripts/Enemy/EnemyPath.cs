﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum EnemyStatus
{
    Entering, Waiting, Exiting
}

public class EnemyPath : MonoBehaviour
{
    public Path path;

    public EnemyStatus currentStatus = EnemyStatus.Entering;

    void Start()
    {
        DOTween.Init();
        DOTween.useSmoothDeltaTime = true;
        DOTween.defaultEaseType = Ease.InOutQuad;
    }

    void Update()
    {  
        if (!DOTween.IsTweening(transform, true) && (currentStatus == EnemyStatus.Exiting || path.destroyAtEnd))
            Kill();
    }

    public void StartPath(Path path)
    {
        this.path = path;

        Tween tween = transform.DOPath(path.enterMoves, path.enterDuration, PathType.CatmullRom, PathMode.TopDown2D).SetDelay(path.enterDelay);
        tween.OnPlay(() => transform.gameObject.SetActive(true));

        if(path.hasExitPath)
        {
            transform.DOPath(path.exitMoves, path.exitDuration, PathType.CatmullRom, PathMode.TopDown2D).SetDelay(path.exitDuration + path.enterDelay + path.exitDelay);
        }

        StartCoroutine(Enter());
    }

    private void Kill()
    {
        transform.DOKill();
        GameObject.Destroy(transform.gameObject);
    }

    private IEnumerator Enter()
    {
        yield return new WaitForSeconds(path.enterDelay + path.enterDuration);
        currentStatus = EnemyStatus.Waiting;
        if (path.hasExitPath)
        {
            StartCoroutine(Exit());
        }
    }

    private IEnumerator Exit()
    {
        yield return new WaitForSeconds(path.exitDelay);
        currentStatus = EnemyStatus.Exiting;
    }
}
