﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpreadAttack : MonoBehaviour, IAttack
{
    public float fireSpeed = 10f;

    public SpriteRenderer enemySprite;

    private BulletPool bulletPool;
    private ShapeProperty enemyShape;

    private Color enemyColor;

    // Use this for initialization
    void Awake()
    {
        bulletPool = FindObjectOfType<BulletPool>();
        enemyShape = GetComponentInParent<ShapeProperty>();
        enemySprite = GetComponent<SpriteRenderer>();

        enemyColor = enemySprite.color;
    }

    public void Attack()
    {
        if (gameObject.activeSelf)
        {
            Vector3 euler = transform.rotation.eulerAngles;

            euler.z -= 30f;
            bulletPool.FireBullet(enemyShape.currentShape, enemyColor, transform.position, Quaternion.Euler(euler), fireSpeed, "Enemy Bullet");
            bulletPool.FireBullet(enemyShape.currentShape, enemyColor, transform.position, transform.rotation, fireSpeed, "Enemy Bullet");
            euler.z += 60f;
            bulletPool.FireBullet(enemyShape.currentShape, enemyColor, transform.position, Quaternion.Euler(euler), fireSpeed, "Enemy Bullet");
        }
    }
}
