﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVisible : MonoBehaviour
{
    //[HideInInspector]
    public bool visible = false;

    private Health enemyHealth;

    void Start()
    {
        enemyHealth = GetComponent<Health>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Background")
        {
            visible = true;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Boundary")
            visible = true;
        else if (coll.tag == "Background")
            visible = false;
    }
}
