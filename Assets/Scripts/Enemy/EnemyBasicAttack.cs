﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBasicAttack : MonoBehaviour, IAttack
{
    private BulletPool bulletPool;
    private ShapeProperty enemyShape;
    private Color enemyColor;

    public float fireSpeed = 10f;

	// Use this for initialization
	void Awake ()
    {
        bulletPool = FindObjectOfType<BulletPool>();
        enemyShape = GetComponentInParent<ShapeProperty>();
        enemyColor = GetComponent<SpriteRenderer>().color;
	}
	
    public void Attack()
    {
        if (gameObject.activeSelf)
        {
            bulletPool.FireBullet(enemyShape.currentShape, enemyColor, transform.position, transform.rotation, fireSpeed, "Enemy Bullet");
        }
    }
}
