﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * Base class for more complex enemies. Includes general-purpose functions and coroutines
 */

public enum EnemyState
{
    Enter, PreAttack, Attack, PostAttack, Exit, None
}

public abstract class EnemyBase : MonoBehaviour
{
    public IAttack[] enemyAttacks;

    protected uint currentAttack = 0;

    private bool coRunning = false;

    protected float rotateSpeed = 0f;

    protected EnemyState currentState;

	// Use this for initialization
	void Awake()
    {
    }

    void Update()
    {
        transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);

        if (!coRunning)
        {
            switch (currentState)
            {
                case EnemyState.Enter:
                    OnEnter();
                    break;

                case EnemyState.Exit:
                    OnExit();
                    break;

                case EnemyState.PreAttack:
                    OnPreAttack();
                    break;

                case EnemyState.Attack:
                    OnAttack();
                    break;

                case EnemyState.PostAttack:
                    OnPostAttack();
                    break;
            }
        }
    }

    protected virtual void OnEnter() { }
    protected virtual void OnExit() { }
    protected virtual void OnPreAttack() { }
    protected virtual void OnAttack() { }
    protected virtual void OnPostAttack() { }

    public IEnumerator Move(Transform t, Vector3 to, float dur, float delay = 0)
    {
        coRunning = true;
        Tween tween = t.DOMove(to, dur);
        if (delay != 0)
            tween.SetDelay(delay);
        yield return new WaitForSeconds(dur + delay);
        coRunning = false;
    }

    public IEnumerator Move(Transform t, Vector3 to, float dur, EnemyState nextState, float delay = 0)
    {
        coRunning = true;
        Tween tween = t.DOMove(to, dur);
        if (delay != 0)
            tween.SetDelay(delay);
        yield return new WaitForSeconds(dur + delay);
        currentState = nextState;
        coRunning = false;
    }

    public IEnumerator Spin(float toSpeed, float dur)
    {
        coRunning = true;
        float currentSpeed = rotateSpeed;
        float elapsed = 0.0f;
        while (elapsed < dur)
        {
            rotateSpeed = Mathf.Lerp(currentSpeed, toSpeed, elapsed / dur);
            elapsed += Time.deltaTime;
            yield return null;
        }
        coRunning = false;
    }

    public IEnumerator Spin(float toSpeed, float dur, EnemyState nextState = EnemyState.None)
    {
        coRunning = true;
        float currentSpeed = rotateSpeed;
        float elapsed = 0.0f;
        while (elapsed < dur)
        {
            rotateSpeed = Mathf.Lerp(currentSpeed, toSpeed, elapsed / dur);
            elapsed += Time.deltaTime;
            yield return null;
        }
        currentState = nextState;
        coRunning = false;
    }

    public IEnumerator Attack(IAttack attack, int times, float rate, EnemyState nextState)
    {
        coRunning = true;
        for(int i = 0; i < times; i++)
        {
            attack.Attack();
            yield return new WaitForSeconds(rate);
        }
        currentState = nextState;
        coRunning = false;
    }
}
