﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    private float initialHealth;
    private bool isHit = false;
    private float initialRed;
    private float initialGreen;
    private float initialBlue;

    private Health enemyHealth;

    private ShapeProperty enemyShape;
    private SpriteRenderer sprite;

    public LevelStats stats;

    private EnemyVisible visible;

    private Color spriteColor;
    private Color hitColor;    

	// Use this for initialization
	void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        enemyHealth = GetComponent<Health>();
        enemyShape = GetComponent<ShapeProperty>();
        visible = GetComponent<EnemyVisible>();

        spriteColor = sprite.color;
        hitColor = new Color(spriteColor.r / 2, spriteColor.g /2 , spriteColor.b / 2, spriteColor.a);

        stats = LevelStats.instance;

        initialHealth = enemyHealth.health;
        initialRed = sprite.color.r;
        initialGreen = sprite.color.g;
        initialBlue = sprite.color.b;
	}
	
	// Update is called once per frame
	void Update()
    {
        if (isHit)
        {
            sprite.color = hitColor;

            isHit = false;
        }
        else
            sprite.color = spriteColor;
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player Bullet")
        {
            var bulletShape = coll.GetComponent<ShapeProperty>();
            if (visible.visible && !enemyHealth.invincible && enemyShape.currentShape == bulletShape.currentShape)
            {
                isHit = true;
                enemyHealth.health--;
            }
            stats.bulletsHit++;
        }
    }
}
