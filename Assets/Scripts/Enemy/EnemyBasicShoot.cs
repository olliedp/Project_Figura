﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBasicShoot : MonoBehaviour
{
    public float fireRate;
    private float fireTime = 0f;

    private IAttack enemyAttack;

    private EnemyVisible vis;

	// Use this for initialization
	void Start ()
    {
        enemyAttack = GetComponent<IAttack>();
        vis = GetComponent<EnemyVisible>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (vis.visible)
        {
            if (fireTime >= fireRate)
            {
                enemyAttack.Attack();
                fireTime = 0;
            }
            else
                fireTime += Time.deltaTime;
        }

        Debug.Log(fireTime);
	}
}
