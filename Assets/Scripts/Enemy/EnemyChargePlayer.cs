﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using System;

public class EnemyChargePlayer : EnemyBase
{
    public float initDur;

    public float chargeRate;
    public float chargeDur;

    // Start and end positions for when the enemy enters
    public float initDelay;
    public Vector3 initStart;
    public Vector3 initEnd;
    

    private float timeSpan = 0;

    private GameObject player;

    // Use this for initialization
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        transform.position = initStart;

        rotateSpeed = 1f;
        currentState = EnemyState.Enter;

        DOTween.Init();
        DOTween.useSmoothDeltaTime = true;
        DOTween.defaultEaseType = Ease.InOutSine;

        StartCoroutine(Move(transform, initEnd, initDur, EnemyState.PreAttack, initDelay));

        Console.WriteLine(GetComponent<IAttack>());
	}
	
	// Update is called once per frame

    protected override void OnPreAttack()
    {
        StartCoroutine(Spin(200f, 0.5f, EnemyState.Attack));
    }

    protected override void OnAttack()
    {
        StartCoroutine(Spin(75f, chargeDur));
        StartCoroutine(Move(transform, player.transform.position, chargeDur, EnemyState.PostAttack));
    }

    protected override void OnPostAttack()
    {
        StartCoroutine(Attack(GetComponent<IAttack>(), 4, 0.25f, EnemyState.PreAttack));
    }
}
