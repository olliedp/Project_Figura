﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotationalAttack : MonoBehaviour, IAttack
{
    public int angle = 90;
    public bool matchRotation = false;
    public int incrementAngle = 0;

    private int startAngle = 0;

    private SpriteRenderer enemySprite;

    private BulletPool bulletPool;
    private ShapeProperty enemyShape;
    private Color enemyColor;

    // Use this for initialization
    void Awake()
    {
        bulletPool = FindObjectOfType<BulletPool>();
        enemyShape = GetComponent<ShapeProperty>();
        enemySprite = GetComponent<SpriteRenderer>();

        if (enemyShape == null)
            enemyShape = GetComponentInParent<ShapeProperty>();

        if (enemySprite == null)
            enemySprite = GetComponentInParent<SpriteRenderer>();

        enemyColor = enemySprite.color;
    }

    public void Attack()
    {
        Quaternion rotation = (matchRotation) ? transform.rotation : Quaternion.Euler(0, 0, startAngle);
        Vector3 euler = rotation.eulerAngles;

        for (float i = startAngle; i < startAngle + 360; i += angle)
        {
            bulletPool.FireBullet(enemyShape.currentShape, enemyColor, transform.position, rotation, 7.5f, "Enemy Bullet");
            euler.z += angle;
            rotation.eulerAngles = euler;
        }
        startAngle += incrementAngle;
    }
}
