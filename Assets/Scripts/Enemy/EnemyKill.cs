﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyKill : MonoBehaviour, IKillable
{
    private PelletPool pellets;
    private ShapeProperty enemyShape;

    private LevelStats stats;

    public int pelletsOnDestroy = 5;
    public bool deactivateOnly = false;

	// Use this for initialization
	void Start()
    {
        pellets = FindObjectOfType<PelletPool>();
        enemyShape = GetComponent<ShapeProperty>();

        stats = LevelStats.instance;
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}

    public void Kill()
    {
        stats.enemyKills++;
        for(int i = 0; i < pelletsOnDestroy; i++)
        {
            pellets.GetPellet(enemyShape.currentShape, transform.position, transform.rotation);
        }
        Score.value += 100;
        if (!deactivateOnly)
            Destroy(gameObject);
        else
            gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        transform.DOKill(); //Get rid of any paths this enemy might be attached to
    }
}
