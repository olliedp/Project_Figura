﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBoss : MonoBehaviour
{
    public GameObject boss;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Spawn());
	}
	
	IEnumerator Spawn()
    {
        yield return new WaitForSeconds(87.5f);
        boss.SetActive(true);
    }
}
