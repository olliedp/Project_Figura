﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Boss1Controller : MonoBehaviour
{
    private float[] xPositions = new float[] { -5f, 0, 5f };
    private float[] yPositions = new float[] { -5f, 0, 5f };
    private int xIndex = 1, yIndex = 1;
    private float rotateSpeed = 0.5f;
    private int currentAttack = 0;
    private BossState currentState = BossState.Enter;
    private enum BossState
    {
        Enter, Rotating, MoveStart, Moving, StartAttack, AttackingCircle, AttackingSquare, AttackingBoth, AttackingCenter, Dying, None
    }
    
    public GameObject square;
    public Health squareHealth;
    public EnemyRotationalAttack[] attacks;
    public GameObject semicircles;
    public EnemyBasicAttack[] semis;
    public Health[] semiHealth;
    public GameObject player;

    private System.Random random;
    private LevelStats stats;
    private EnemyRotationalAttack squareCrossAttack;
    private EnemyRotationalAttack squareCircleAttack;   //weird i know

    public PlayerLives playerLives;

	// Use this for initialization
	void Start ()
    {
        semis = semicircles.GetComponentsInChildren<EnemyBasicAttack>();
        semiHealth = semicircles.GetComponentsInChildren<Health>();

        squareCrossAttack = attacks[0];
        squareCircleAttack = attacks[1];
        squareHealth = square.GetComponent<Health>();

        DOTween.Init();
        DOTween.useSmoothDeltaTime = true;
        DOTween.defaultEaseType = Ease.InOutQuad;

        Tween t = transform.DOMove(new Vector3(0, 0, 0), 8);
        t.OnKill(() => StartCoroutine(StartRotation(1.5f, 3f, () => currentState = BossState.MoveStart, () => PlayerShoot.canShoot = true)));

        random = new System.Random();

        stats = LevelStats.instance;

        playerLives = PlayerLives.instance;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if((currentState != BossState.Dying))
        {
            square.transform.Rotate(0, 0, rotateSpeed / 2);
            semicircles.transform.Rotate(0, 0, -rotateSpeed);
        }

        if (squareHealth.health <= 0 && currentState != BossState.Dying)
        {
            playerLives.invincible = true;
            stats.timerStart = false;
            currentState = BossState.Dying;
            StartCoroutine(Shake(square, () => Destroy(gameObject)));
            StartCoroutine(Explode(5f, squareHealth.killScript, true));
            float delay = 1f;
            foreach (var semi in semiHealth)
            {
                StartCoroutine(Shake(semi.gameObject));
                if (semi.gameObject.activeSelf)
                    StartCoroutine(Explode(delay++, semi.killScript));
            }
            transform.DOKill(false);
        }

        

        if(currentState == BossState.MoveStart)
        {
            currentState = BossState.Moving;

            Tween t;

            if (currentAttack % 5 == 4)
                t = transform.DOMove(new Vector3(0, 0), 2f);
            else
            {
                int randX = random.Next(3);
                int randY = random.Next(3);
                while(randX == xIndex && randY == yIndex)
                {
                    randX = random.Next(3);
                    randY = random.Next(3);
                }
                xIndex = randX;
                yIndex = randY;

                t = transform.DOMove(new Vector3(xPositions[randX], yPositions[randY]), 2f);
            }
            t.OnKill(() => Attack());


            if (currentAttack % 5 == 2)
                StartCoroutine(Expand(1f, 2f));
            else if (currentAttack != 0 && currentAttack % 5 == 4)
                StartCoroutine(Expand(5f, 2f));
            else if (currentAttack != 0 && currentAttack % 5 == 0)
                StartCoroutine(Expand(-6f, 2f));
        }
	}

    private void Attack()
    {
        switch(currentAttack % 5)
        {
            case 0:
                currentState = BossState.AttackingCircle;
                StartCoroutine(StartAttackCircle());
                break;

            case 1:
                goto case 0;

            case 2:
                currentState = BossState.AttackingBoth;
                StartCoroutine(StartAttackCircle());
                StartCoroutine(StartAttackSquare(3f));
                break;

            case 3:
                goto case 2;

            case 4:
                //StartCoroutine(StartRotation(2f, 1f, () => currentState = BossState.AttackingCenter));
                currentState = BossState.AttackingCenter;
                //              StartCoroutine(Expand(3f, 0.05f));
                //StartCoroutine(StartRotation(0, 1f, null));
                StartCoroutine(StartAttackCenter());
                break;

        }
        currentAttack++;
    }

    private IEnumerator StartRotation(float speed, float time, params Action[] nextActions)
    {
        //currentState = BossState.Rotating;
        for(float i = 0; i < 3f; i += Time.deltaTime)
        {
            rotateSpeed = Mathf.Lerp(0.5f, 1.5f, i / 3f);
            yield return new WaitForEndOfFrame();
        }
        stats.timerStart = true;
        foreach (var a in nextActions)
            a();
    }

    private IEnumerator StartAttackCircle()
    {
        float elapsedTime = 0f;

        while(elapsedTime < 3f && currentState != BossState.Dying)
        {
            foreach (var attack in semis)
                if(attack.gameObject != null)
                    attack.Attack();

            elapsedTime += 0.125f;
            yield return new WaitForSeconds(0.125f);
        }

        if (currentState != BossState.Dying && currentState != BossState.MoveStart && currentState != BossState.Moving)
            currentState = BossState.MoveStart;
    }

    private IEnumerator StartAttackSquare(float time)
    {
        float elapsedTime = 0f;

        while (elapsedTime < time && currentState != BossState.Dying)
        {
            squareCrossAttack.Attack();

            elapsedTime += 0.125f;
            yield return new WaitForSeconds(0.125f);
        }

        if (currentState != BossState.AttackingCenter && currentState != BossState.Dying && currentState != BossState.MoveStart && currentState != BossState.Moving)
            currentState = BossState.MoveStart;
    }

    private IEnumerator StartAttackCenter()
    {
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ShootAllOver());
        yield return new WaitForSeconds(1f);
        StartCoroutine(StartAttackSquare(2.5f));
        yield return new WaitForSeconds(1f);
        Move();
        yield return new WaitForSeconds(2f);
        StartCoroutine(ShootAllOver());
        yield return new WaitForSeconds(1f);
        StartCoroutine(StartAttackSquare(2.5f));
        yield return new WaitForSeconds(1f);
        Move();
        yield return new WaitForSeconds(2f);
        StartCoroutine(ShootAllOver());
        yield return new WaitForSeconds(1f);
        StartCoroutine(StartAttackSquare(2.5f));
        yield return new WaitForSeconds(1f);
        Move();
        yield return new WaitForSeconds(2f);
        StartCoroutine(ShootAllOver());
        yield return new WaitForSeconds(1f);

        if (currentState != BossState.Dying && currentState != BossState.MoveStart && currentState != BossState.Moving)
            currentState = BossState.MoveStart;

    }

    private IEnumerator Expand(float speed, float duration)
    {
        float elapsed = 0;
        while(elapsed < duration)
        {
            elapsed += Time.deltaTime;

            foreach (var semi in semis)
                semi.transform.Translate(0f, speed * Time.deltaTime, 0f);

            yield return null;
        }
    }

    private IEnumerator Shake(GameObject obj, Action endAction = null)
    {
        yield return new WaitForSeconds(1f);
        
        if (obj != null)
        {
            var startPos = obj.transform.position;

            float elapsedTime = 0f;

            while (elapsedTime < 5f && obj != null)
            {
                obj.transform.position = startPos + ((Vector3)UnityEngine.Random.insideUnitCircle * 0.1f);

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            if (endAction != null)
                endAction();
        }
    }

    private IEnumerator Explode(float delay, IKillable killScript, bool completeLevel = false)
    {
        yield return new WaitForSeconds(delay);

        killScript.Kill();

        if (completeLevel)
            stats.levelComplete = true;
    }

    private void Move()
    {
        int randX = random.Next(3);
        int randY = random.Next(3);
        while(randX == xIndex && randY == yIndex)
        {
            randX = random.Next(3);
            randY = random.Next(3);
        }
        xIndex = randX;
        yIndex = randY;
        transform.DOMove(new Vector3(xPositions[randX], yPositions[randY]), 2f);
    }

    private IEnumerator ShootAllOver()
    {
        float elapsedTime = 0;
        while (elapsedTime < 1f && currentState != BossState.Dying)
        {
            squareCircleAttack.Attack();

            elapsedTime += 0.25f;
            yield return new WaitForSeconds(0.25f);
        }
    }
}
