﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Boss2Controller : MonoBehaviour
{
    private int charges = 2;    //number of regular attacks before doing special stuff

    public GameObject player;

    public GameObject triangles;
    private EnemyRotationalAttack[] triBasicAttacks;
    private Boss2SpikeMove[] spikeMoves;

    private float rotateSpeed = 1f;

    private enum State
    {
        ShootingTris, SpeedUp, Charge, SlowDown, Contract, ShootSpikes, None
    }

    private State currentState = State.ShootingTris;
    private bool coRunning = false;

	// Use this for initialization
	void Start ()
    {
        triBasicAttacks = triangles.GetComponentsInChildren<EnemyRotationalAttack>();
        spikeMoves = triangles.GetComponentsInChildren<Boss2SpikeMove>();

        DOTween.Init();
        DOTween.useSmoothDeltaTime = true;
        DOTween.defaultEaseType = Ease.InOutQuad;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!coRunning)
            switch(currentState)
            {
                case State.ShootingTris:
                    StartCoroutine(TriBasicShoot());
                    currentState = State.SpeedUp;
                    break;

                case State.SpeedUp:
                    StartCoroutine(RotationSpeed(1f, 5f));
                    foreach(var spike in spikeMoves)
                        StartCoroutine(Expand(spike.transform, 1f, 1f));
                    currentState = State.Charge;
                    break;

                case State.Charge:
                    if (charges > 0)
                        StartCoroutine(Charge(player.transform.position));
                    else
                        StartCoroutine(Charge(new Vector2(0, 0)));
                    currentState = State.SlowDown;
                    break;

                case State.SlowDown:
                    StartCoroutine(RotationSpeed(5f, 1f));
                    foreach(var spike in spikeMoves)
                        StartCoroutine(Expand(spike.transform, -1f, 1f));
                    if (charges > 0)
                    {
                        charges--;
                        currentState = State.ShootingTris;
                    }
                    else
                        currentState = State.Contract;
                    break;

                case State.Contract:
                    StartCoroutine(RotationSpeed(1f, 0f));
                    foreach (var spike in spikeMoves)
                        StartCoroutine(Expand(spike.transform, -0.5f, 1));
                    currentState = State.ShootSpikes;
                    break;

                case State.ShootSpikes:
                    foreach (var spike in spikeMoves)
                        StartCoroutine(Expand(spike.transform, 5f, 1));
                    break;
            }

        transform.Rotate(0, 0, rotateSpeed);
	}

    private IEnumerator TriBasicShoot()
    {
        coRunning = true;
        for (int i = 0; i < 7.5; i++)
        {
            foreach (var v in triBasicAttacks)
            {
                if(v.gameObject.activeSelf)
                    v.Attack();
                yield return new WaitForSeconds(0.0125f);
            }
            yield return new WaitForSeconds(0.0125f);
        }
        coRunning = false;
    }

    private IEnumerator RotationSpeed(float start, float end)
    {
        coRunning = true;
        float elapsed = 0.0f;
        while(elapsed < 1f)
        {
            rotateSpeed = Mathf.Lerp(start, end, elapsed / 1f);
            elapsed += Time.deltaTime;
            yield return null;
        }
        rotateSpeed = end;
        coRunning = false;
    }

    private IEnumerator Expand(Transform t, float amt, float time)
    {
        coRunning = true;
        float elapsed = 0.0f;
        while (elapsed < time)
        {
            
            t.Translate(0, amt * Time.deltaTime, 0);
            elapsed += Time.deltaTime;
            yield return null;
        }
        coRunning = false;
    }

    private IEnumerator ExpandIndividual(float amt, float time, float delay)
    {
        coRunning = true;
        foreach (var spike in spikeMoves)
        {
            StartCoroutine(Expand(spike.transform, amt, time));
            yield return new WaitForSeconds(delay);
        }  
        coRunning = false;
    }

    private IEnumerator Charge(Vector2 position)
    {
        coRunning = true;
        Tween t = transform.DOMove(position, 2f);
        yield return new WaitForSeconds(2f);
        coRunning = false;
    }
}
