﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Disappear after a given amount of time
 */

public class PelletDisappear : MonoBehaviour
{
    public float timeEnabled = 2;

    private PelletPool pool;
    private SpriteRenderer sprite;

    void Start()
    {
        pool = FindObjectOfType<PelletPool>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        float elapsedTime = 0;
        Color white = Color.white;
        while(elapsedTime < timeEnabled)
        {
            white.a = 1f - Mathf.Lerp(0, 1, elapsedTime / timeEnabled);
            elapsedTime += Time.deltaTime;
            GetComponent<SpriteRenderer>().color = white;
            yield return null;
        }
        pool.Decrement();
        gameObject.SetActive(false);
    }
}
