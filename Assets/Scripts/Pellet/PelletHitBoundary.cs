﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelletHitBoundary : MonoBehaviour
{
    private PelletPool pelletPool;

    void Start()
    {
        pelletPool = FindObjectOfType<PelletPool>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == "Boundary")
        {
            gameObject.SetActive(false);
            pelletPool.Decrement();
        }
    }
}
