﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelletEnabled : MonoBehaviour
{
    new private Rigidbody2D rigidbody;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        //gameObject.SetActive(false);
    }

    void Update()
    {
        rigidbody.AddForce(-rigidbody.velocity);
    }

    void OnEnable()
    {
        rigidbody.AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(0f, 1f)) * 3, ForceMode2D.Impulse);
    }
}
