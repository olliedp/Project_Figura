﻿using System.Collections.Generic;
using UnityEngine;

public class PelletPool : MonoBehaviour
{
    public uint poolSize;
    public int activePelletCout;

    public Sprite square, circle, triangle;
    public GameObject pellet;

    public GameObject player;
    private List<GameObject> pelletList;
    private List<PlayerAttraction> attrList;

    // Use this for initialization
    void Start()
    {
        pellet.SetActive(false);
        pelletList = new List<GameObject>();
        attrList = new List<PlayerAttraction>();

        for (int i = 0; i < poolSize; i++)
            AddPellet();
    }

    public void GetPellet(Shape shape, Vector3 position, Quaternion rotation)
    {
        if (activePelletCout >= pelletList.Count - 1)
            for (int i = 0; i < (pelletList.Count / 10); i++)
                AddPellet();

        for (int i = 0; i < pelletList.Count; i++)
            if (!pelletList[i].activeSelf)
            {
                pelletList[i].transform.position = position;
                pelletList[i].transform.rotation = rotation;
                pelletList[i].GetComponent<ShapeProperty>().currentShape = shape;
                var spriteRender = pelletList[i].GetComponent<SpriteRenderer>();
                switch(shape)
                {
                    case Shape.Square:
                        spriteRender.sprite = square;
                        break;
                    case Shape.Circle:
                        spriteRender.sprite = circle;
                        break;
                    case Shape.Triangle:
                        spriteRender.sprite = triangle;
                        break;
                }
                pelletList[i].SetActive(true);
                break;
            }
        activePelletCout++;
    }

    private void AddPellet()
    {
        GameObject pelletObject = Instantiate(pellet, transform.position, transform.rotation);
        pelletList.Add(pelletObject);
        pelletObject.GetComponent<PlayerAttraction>().player = player;
        pelletObject.SetActive(false);
    }

    public void Decrement()
    {
        activePelletCout--;
    }
}
