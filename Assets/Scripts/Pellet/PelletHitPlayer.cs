﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelletHitPlayer : MonoBehaviour
{
    private PelletPool pelletPool;
    private ShapeProperty pelletShape;

	// Use this for initialization
	void Start ()
    {
        pelletPool = FindObjectOfType<PelletPool>();
        pelletShape = GetComponent<ShapeProperty>();	
	}
	
	void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            var playerShape = coll.GetComponentInParent<ShapeProperty>();
            if (pelletShape.currentShape == playerShape.currentShape)
            {
                Score.value += 10;
                pelletPool.Decrement();
                gameObject.SetActive(false);
            }
        }
    }
}
