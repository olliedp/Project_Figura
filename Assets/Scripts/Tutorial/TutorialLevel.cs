﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TutorialLevel : MonoBehaviour
{
    public enum TutorialPoint
    {
        Start, Arrows, Tab, Waves, Pellets, EShoot, End
    }
    public TutorialPoint point = TutorialPoint.Start;

    public ShapeProperty playerShape;

    public Wave waveSquare, waveCircle, waveTriangle;
    public Wave shootSquare, shootCircle, shootTriangle;

    private string info = "WASD to move";
    private Text uiText;

	// Use this for initialization
	void Start ()
    {
        uiText = GameObject.FindObjectOfType<Text>();
        uiText.text = info;
        Color color = uiText.color;
        color.a = 0;
        uiText.color = color;
        StartCoroutine(FadeInText());
	}
	
	// Update is called once per frame
	void Update ()
    {
		switch(point)
        {
            case TutorialPoint.Start:
                if (Input.GetKeyDown(KeyCode.W) ||
                   Input.GetKeyDown(KeyCode.A) ||
                   Input.GetKeyDown(KeyCode.S) ||
                   Input.GetKeyDown(KeyCode.D))
                {
                    point = TutorialPoint.Arrows;
                    goto default;
                }
                break;

            case TutorialPoint.Arrows:
                if(Input.GetKeyDown(KeyCode.UpArrow) ||
                    Input.GetKeyDown(KeyCode.DownArrow) ||
                    Input.GetKeyDown(KeyCode.LeftArrow) ||
                    Input.GetKeyDown(KeyCode.RightArrow))
                {
                    point = TutorialPoint.Tab;
                    goto default;
                }
                break;

            case TutorialPoint.Tab:
                if(Input.GetKeyDown(KeyCode.Tab) && playerShape.currentShape == Shape.Square)
                {
                    Instantiate(waveSquare.gameObject);
                    Instantiate(waveCircle.gameObject);
                    Instantiate(waveTriangle.gameObject);
                    point = TutorialPoint.Waves;
                    StartCoroutine(Wait(3, TutorialPoint.Pellets));
                    goto default;
                }
                break;

            case TutorialPoint.Waves:
                break;

            case TutorialPoint.Pellets:
                if(DOTween.TotalPlayingTweens() == 0 && GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
                {
                    Instantiate(shootSquare);
                    Instantiate(shootCircle);
                    Instantiate(shootTriangle);
                    StartCoroutine(Wait(1f, TutorialPoint.EShoot));
                }
                break;

            case TutorialPoint.EShoot:
                if(GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
                {
                    point = TutorialPoint.End;
                    goto default;
                }
                break;

            case TutorialPoint.End:
                break;

            default:
                StartCoroutine(FadeOutText(point));
                break;
        }

        Debug.Log(point);
	}

    IEnumerator FadeInText()
    {
        for(float i = 0.05f; i < 1; i += 0.05f)
        {
            SetTextAlpha(i);

            yield return null;
        }
    }

    IEnumerator FadeOutText(TutorialPoint nextPoint)
    {
        for (float i = 1f; i >= 0; i -= 0.05f)
        {
            SetTextAlpha(i);

            yield return null;
        }
        SetTextAlpha(0f);
        point = nextPoint;
        ChangeTutString(nextPoint);
        StartCoroutine(FadeInText());
    }

    private void ChangeTutString(TutorialPoint t)
    {
        switch(t)
        {
            case TutorialPoint.Arrows:
                uiText.text = "Arrow keys to shoot";
                break;

            case TutorialPoint.Tab:
                uiText.text = "Tab to change shape";
                break;

            case TutorialPoint.Waves:
                uiText.text = "Shoot at enemies that match your shape";
                break;

            case TutorialPoint.Pellets:
                uiText.text = "Collect matching pellets to gain power";
                break;

            case TutorialPoint.EShoot:
                uiText.text = "Non-matching enemy bullets don't deal damage";
                break;

            case TutorialPoint.End:
                uiText.text = "Enjoy the game!";
                break;
        }
    }

    private void SetTextAlpha(float a)
    {
        Color color = uiText.color;
        color.a = a;
        uiText.color = color;
    }

    private IEnumerator Wait(float seconds, TutorialPoint nextPoint)
    {
        yield return new WaitForSeconds(seconds);
        StartCoroutine(FadeOutText(nextPoint));
    }
}
