﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectPool : MonoBehaviour
{
    public uint size; 

    public GameObject pooledObject; //the object we're pooling

    protected List<GameObject> objPool;   //pool of objects;

    private uint _activeObjects = 0; //how many objects are currently active

	protected virtual void Awake()
    {
        objPool = new List<GameObject>();

        //fill the pool with inactive objects
        for(int i = 0; i < size; i++)
        {
            var gameObj = GameObject.Instantiate(pooledObject);
            gameObj.SetActive(false);

            objPool.Add(gameObj);
        }
    }

    //activates an inactive pooled object and returns the index of the activated object
    public virtual int Pull()
    {
        int index = FindInactiveObject();

        if(index != -1)
            objPool[index].SetActive(true);

        else
        {
            AddObject();
            objPool[(int)size - 1].SetActive(true);
        }

        return index;
    }

    //same as Pull(), but set the object's position
    public virtual int Pull(Vector2 position)
    {
        int index = Pull();
        objPool[index].transform.position = position;

        return index;
    }

    public virtual int Pull(Vector2 position, Quaternion rotation)
    {
        int index = Pull();
        objPool[index].transform.position = position;
        objPool[index].transform.rotation = rotation;

        return index;
    }

    public virtual int Pull(Vector2 position, Quaternion rotation, string tag)
    {
        int index = Pull();
        objPool[index].transform.position = position;
        objPool[index].transform.rotation = rotation;
        objPool[index].tag = tag;

        return index;
    }

    protected int FindInactiveObject()
    {
        int pos = 0;

        //activate and return the first inactive object in the list
        for (pos = 0; pos < size; pos++)
        {
            if (!objPool[pos].activeSelf)
            {
                return pos;
            }
        }

        return -1;
    }

    protected virtual void AddObject()
    {
        var gameObj = GameObject.Instantiate(pooledObject);
        gameObj.SetActive(false);
        objPool.Add(gameObj);
        size++;
    }
}
