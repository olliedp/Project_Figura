﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHitEnemy : MonoBehaviour
{
    private BulletPool pool;
    private ShapeProperty bulletShape;

	// Use this for initialization
	void Awake()
    {
        pool = FindObjectOfType<BulletPool>();
        bulletShape = GetComponent<ShapeProperty>();
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (tag == "Player Bullet" && coll.tag == "Enemy")
        {
            gameObject.SetActive(false);
        }
    }
}
