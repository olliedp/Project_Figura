﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMoveReverse : MonoBehaviour
{
    public float moveSpeed;

    private static BulletPool bulletPool;
    private float speedX, speedY;

    private LevelStats stats;

    void Awake()
    {
        if (bulletPool == null)
            bulletPool = FindObjectOfType<BulletPool>();

        stats = FindObjectOfType<LevelStats>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, moveSpeed * Time.deltaTime, 0);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (gameObject.activeSelf && coll.tag == "Boundary")
        {
            if (tag == "Player Bullet")
                stats.bulletsMissed++;
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (gameObject.activeSelf && coll.tag == "Background" && tag == "Enemy Bullet")
        {
            gameObject.SetActive(false);
        }
    }
}
