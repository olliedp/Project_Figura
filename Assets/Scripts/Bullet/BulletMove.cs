﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    private LevelStats stats;
    private BulletSpeed speed;

    void Awake()
    {
        stats = FindObjectOfType<LevelStats>();
        speed = GetComponent<BulletSpeed>();
    }

	// Update is called once per frame
	void Update ()
    {
        transform.Translate(0, speed.value * Time.deltaTime, 0);
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(gameObject.activeSelf && coll.tag == "Boundary")
        {
            if (tag == "Player Bullet")
            {
                stats.bulletsMissed++;
            }
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if(gameObject.activeSelf && coll.tag == "Background" && tag == "Enemy Bullet")
        {
            gameObject.SetActive(false);
        }
    }
}
