﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDisabled : MonoBehaviour
{
    private BulletPool bulletPool;

    void Update()
    {
        if(bulletPool == null)
            bulletPool = FindObjectOfType<BulletPool>();
    }

    void OnDisable()
    {
        if(bulletPool != null)
            bulletPool.Decrement();
    }
}
