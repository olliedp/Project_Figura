﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPoolNew : ObjectPool
{
    protected List<ShapeProperty> bulletShapes;
    protected List<SpriteRenderer> bulletSprites;
    protected List<BulletSpeed> bulletSpeeds;

	new void Awake()
    {
        base.Awake();

        bulletShapes = new List<ShapeProperty>();
        foreach (var bullet in objPool)
            bulletShapes.Add(bullet.GetComponent<ShapeProperty>());

        bulletSprites = new List<SpriteRenderer>();
        foreach (var bullet in objPool)
            bulletSprites.Add(bullet.GetComponent<SpriteRenderer>());

        bulletSpeeds = new List<BulletSpeed>();
        foreach (var bullet in objPool)
            bulletSpeeds.Add(bullet.GetComponent<BulletSpeed>());
    }

	public int Pull(Vector2 position, Quaternion rotation, string tag, Shape shape)
    {
        int index = Pull(position, rotation, tag);
        bulletShapes[index].currentShape = shape;

        return index;
    }

    public int Pull(Vector2 position, Quaternion rotation, string tag, Shape shape,  Color color)
    {
        int index = Pull(position, rotation, tag);
        bulletShapes[index].currentShape = shape;
        bulletSprites[index].color = color;

        return index;
    }

    public int Pull(Vector2 position, Quaternion rotation, string tag, Shape shape,  Color color, float speed)
    {
        int index = Pull(position, rotation, tag);
        bulletShapes[index].currentShape = shape;
        bulletSprites[index].color = color;
        bulletSpeeds[index].value = speed;

        return index;
    }

    protected override void AddObject()
    {
        base.AddObject();
        bulletShapes.Add(objPool[(int)size - 1].GetComponent<ShapeProperty>());
        bulletSprites.Add(objPool[(int)size - 1].GetComponent<SpriteRenderer>());
        bulletSpeeds.Add(objPool[(int)size - 1].GetComponent<BulletSpeed>());
    }
}
