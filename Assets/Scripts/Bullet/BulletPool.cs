﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    public uint poolSize;
    public GameObject bullet;

    private List<GameObject> bulletList;
    private List<ShapeProperty> bulletShapes;
    private List<BulletSpeed> bulletSpeeds;
    private List<SpriteRenderer> bulletSprites;
    public int activeBulletCount;

	// Use this for initialization
	void Awake ()
    {
        bulletList = new List<GameObject>();
        bulletShapes = new List<ShapeProperty>();
        bulletSpeeds = new List<BulletSpeed>();
        bulletSprites = new List<SpriteRenderer>();

	    for(int i = 0; i < poolSize; i++)
            AddBullet();
	}

	public void FireBullet(Shape shape, Color color, Vector3 position, Quaternion rotation, float speed, string tag)
    {
        if(activeBulletCount >= bulletList.Count - 1)
            for (int i = 0; i < (bulletList.Count / 2); i++)
                AddBullet();

        for (int i = 0; i < bulletList.Count; i++)
            if (!bulletList[i].activeSelf)
            {
                bulletList[i].SetActive(true);
                bulletList[i].transform.position = position;
                bulletList[i].transform.rotation = rotation;
                bulletList[i].tag = tag;

                bulletShapes[i].currentShape = shape;

                bulletSpeeds[i].value = speed;

                bulletSprites[i].color = color;

                break;
            }
        activeBulletCount++;
    }

    private void AddBullet()
    {
        GameObject bulletObject = Instantiate(bullet, transform.position, transform.rotation);
        bulletList.Add(bulletObject);
        bulletShapes.Add(bulletObject.GetComponent<ShapeProperty>());
        bulletSpeeds.Add(bulletObject.GetComponent<BulletSpeed>());
        bulletSprites.Add(bulletObject.GetComponent<SpriteRenderer>());
        bulletObject.SetActive(false);
    }

    public void Decrement()
    {
        activeBulletCount--;
    }
}
