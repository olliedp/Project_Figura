﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChangeShape : MonoBehaviour
{
    private ShapeProperty shp;
    private Shape currentShape;

    private SpriteRenderer sprite;
    private Sprite square, circle, triangle;

	// Use this for initialization
	void Awake()
    {
        shp =  GetComponentInParent<ShapeProperty>();
        currentShape = shp.currentShape;

        sprite = GetComponent<SpriteRenderer>();
        square = Resources.Load<Sprite>("Square");
        circle = Resources.Load<Sprite>("Circle");
        triangle = Resources.Load<Sprite>("Triangle");
    }
	
	// Update is called once per frame
	void Update()
    {
        if (currentShape != shp.currentShape)
        {
            switch (shp.currentShape)
            {
                case Shape.Square:
                    sprite.sprite = square;
                    break;
                case Shape.Circle:
                    sprite.sprite = circle;
                    break;
                case Shape.Triangle:
                    sprite.sprite = triangle;
                    break;
            }

            currentShape = shp.currentShape;
        }
        
	}

    private void OnEnable()
    {
        switch (shp.currentShape)
        {
            case Shape.Square:
                sprite.sprite = square;
                break;
            case Shape.Circle:
                sprite.sprite = circle;
                break;
            case Shape.Triangle:
                sprite.sprite = triangle;
                break;
        }
    }
}
