﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionRotate : MonoBehaviour
{
    //Use this to rotate the player
    private Vector3 euler;

	// Use this for initialization
	void Start ()
    {
        euler = transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update ()
    {
        float horiz = Input.GetAxisRaw("Fire Horizontal");
        float vert = Input.GetAxisRaw("Fire Vertical");
        if ((horiz > 0.5 || horiz < -0.5) && (vert > 0.5 || vert < -0.5))
        {
            
                if(horiz > 0 && vert > 0)
                    euler.z = 315;

                else if(horiz < 0 && vert < 0)
                    euler.z = 135;

                else
                    euler.z = (horiz > 0) ? 225 : 45;
        }

        else if (horiz > 0.5 || horiz < -0.5)
            euler.z = (horiz > 0) ? 270 : 90;

        else if (vert > 0.5 || vert < -0.5)
            euler.z = (vert > 0) ? 0 : 180;

        /*
        if (Input.GetKeyDown(KeyCode.UpArrow))
            euler.z = 0;
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            euler.z = 180;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            euler.z = 90;
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            euler.z = 270;
            */

        transform.eulerAngles = euler;
	}
}
