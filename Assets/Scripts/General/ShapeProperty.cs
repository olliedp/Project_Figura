﻿using UnityEngine;

public enum Shape
{
    Square = 0, Circle, Triangle
}

public class ShapeProperty : MonoBehaviour
{
    public Shape currentShape;
}
