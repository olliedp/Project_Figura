﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Wave : MonoBehaviour
{
    public Vector3 startPosition;

    [SerializeField]
    private Path initialPath;

    public GameObject[] enemies;

    public float xOffset;
    public float yOffset;

    public float startDelay;
    public float delayEach;

    private float elapsedTime = 0;

    void Start()
    {
        DOTween.Init();
        DOTween.defaultEaseType = Ease.InOutQuad;
        DOTween.useSmoothDeltaTime = true;
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(startDelay);

        float startX = startPosition.x;
        float startY = startPosition.y;

        Path path = initialPath;

        for (int i = 0; i < enemies.Length; i++)
        {
            var enemyObject = Instantiate(enemies[i]);

            enemyObject.transform.position = new Vector3(startX, startY);
            enemyObject.AddComponent<EnemyPath>();

            enemyObject.GetComponent<EnemyPath>().StartPath(path);

            startX += xOffset;
            startY += yOffset;

            path.Shift(xOffset, yOffset);

            yield return new WaitForSeconds(delayEach);
        }
    }
}
