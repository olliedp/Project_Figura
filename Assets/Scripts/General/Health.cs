﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int health = 10;
    public bool invincible = false;
    public bool killOnZero = true;
    
    [SerializeField]
    public IKillable killScript;

    void Start()
    {
        killScript = GetComponent(typeof(IKillable)) as IKillable;
    }

    void Update()
    {
        if (health <= 0 && killOnZero && killScript != null)
        {
            killScript.Kill();
            gameObject.SetActive(false);
        }
    }
}
