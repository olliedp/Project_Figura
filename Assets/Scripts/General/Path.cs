﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Path
{
    public Vector3[] enterMoves;
    public float enterDelay;
    public float enterDuration;

    public Vector3[] exitMoves;
    public float exitDelay;
    public float exitDuration;
    public bool hasExitPath;

    public bool destroyAtEnd;

    public void Shift(float shiftX, float shiftY)
    {
        for (int i = 0; i < enterMoves.Length; i++)
        {
            enterMoves[i].x += shiftX;
            enterMoves[i].y += shiftY;
        }

        if (hasExitPath)
            for (int i = 0; i < exitMoves.Length; i++)
            {
                exitMoves[i].x += shiftX;
                exitMoves[i].y += shiftY;
            }
    }

    public void ShiftY(float shift)
    {
        for (int i = 0; i < enterMoves.Length; i++)
            enterMoves[i].y += shift;

        if (hasExitPath)
            for (int i = 0; i < exitMoves.Length; i++)
                exitMoves[i].y += shift;
    }
}
