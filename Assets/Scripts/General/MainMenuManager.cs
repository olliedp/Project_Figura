﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public int select = 0;

    public Text logo, start, option, exit, loading;

    public AudioSource menuMusic;
    public AudioClip levelMusic;

    public Image fadeCover;
    public GameObject loadSpin;

    private bool delay;
    private static bool sceneLoading = false;
    public static bool SceneLoading
    {
        get
        {
            return sceneLoading;
        }
    }
    private AudioSource audio;

	// Use this for initialization
	void Start ()
    {
        sceneLoading = false;

        AudioManager.instance.Play();

        start.color = Color.white;
        option.color = Color.gray;
        exit.color = Color.gray;

        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!sceneLoading)
        {
            if (Input.GetAxis("Vertical") == 0 && Input.GetAxis("Fire Vertical") == 0)
            {
                delay = false;
                StopAllCoroutines();
            }
            if (!delay && (Input.GetAxis("Vertical") > 0 || Input.GetAxis("Fire Vertical") > 0))
            {
                audio.Play();
                select = (select <= 0) ? 2 : select - 1;
                Select();
                StartCoroutine(SelectionDelay());
            }
            if (!delay && (Input.GetAxis("Vertical") < 0 || Input.GetAxis("Fire Vertical") < 0))
            {
                audio.Play();
                select++;
                Select();
                StartCoroutine(SelectionDelay());
            }
            if (Input.GetAxis("Confirm") > 0)
                Confirm();
        }
	}

    private void Select()
    {
        start.color = Color.gray;
        option.color = Color.gray;
        exit.color = Color.gray;
    
        switch (select % 3)
        {
            case 0:
                start.color = Color.white;
                break;
            case 1:
                option.color = Color.white;
                break;
            case 2:
                exit.color = Color.white;
                break;
        }
    }

    private void Confirm()
    {
        switch(select % 3)
        {
            case 0:
                AudioManager.instance.SetMusic(levelMusic);
                AudioManager.instance.Pause();
                StartCoroutine(LoadGame());
                break;

            case 1:
                SceneManager.LoadScene("Options");
                break;

            case 2:
                Application.Quit();
                break;
        }
    }

    private IEnumerator SelectionDelay()
    {
        delay = true;
        yield return new WaitForSeconds(1f);
        delay = false;
    }

    private IEnumerator LoadGame()
    {
        Debug.Log("Loading???");
        sceneLoading = true;
        StartCoroutine(FadeTo(1f, 1f));

        yield return new WaitForSeconds(1f);

        logo.gameObject.SetActive(false);
        start.gameObject.SetActive(false);
        option.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);

        loading.gameObject.SetActive(true);
        loadSpin.SetActive(true);

        fadeCover.gameObject.SetActive(false);

        loading.color = Color.white;

        AsyncOperation async = SceneManager.LoadSceneAsync("Level 1");
        sceneLoading = false;
        yield return async;
    }

    public IEnumerator FadeTo(float to, float time)
    {
        float elapsed = 0.0f;
        Color currentColor = fadeCover.color;
        float currentA = 0.0f;

        while (elapsed < time)
        {
            currentColor.a = Mathf.Lerp(currentA, to, elapsed / time);
            fadeCover.color = currentColor;
            elapsed += Time.deltaTime;
            yield return null;
        }
    }
}
