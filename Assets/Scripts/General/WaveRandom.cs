﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class WaveRandom : MonoBehaviour
{
    public Vector3 startPosition;

    public Path initialPath;

    public GameObject[] enemies;

    public float rangeX;
    public float rangeY;

    public float startDelay;
    public float delayEach;

    private float elapsedTime = 0;

    void Start()
    {
        DOTween.Init();
        DOTween.defaultEaseType = Ease.InOutQuad;

        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(startDelay);

        float startX = startPosition.x;
        float startY = startPosition.y;

        for (int i = 0; i < enemies.Length; i++)
        {
            Path path = initialPath;

            float randX = Random.Range(-rangeX, rangeX);
            float randY = Random.Range(-rangeY, rangeY);

            var enemyObject = Instantiate(enemies[i], new Vector3(startX + randX, startY + randY), enemies[i].transform.rotation);
            //enemyObject.SetActive(false);
            
            enemyObject.AddComponent<EnemyPath>();
            path.Shift(randX, randY);
            enemyObject.GetComponent<EnemyPath>().StartPath(path);
            path.Shift(-randX, -randY);

            yield return new WaitForSeconds(delayEach);
        }
    }
}
