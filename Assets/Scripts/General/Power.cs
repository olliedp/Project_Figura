﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Power : MonoBehaviour
{
    public int power = 0;

    public int powerLevel = 1;

    public Slider powerSlider;

    void Start()
    {
    }

    void Update()
    {
        if (power >= 50 && power < 150)
            powerLevel = 2;
        else if (power >= 150)
            powerLevel = 3;
        else
            powerLevel = 1;

//        powerSlider.value = power;
    }
}
